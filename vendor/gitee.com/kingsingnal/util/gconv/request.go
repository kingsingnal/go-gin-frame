package gconv

import "net/http"

//获取参数，不支持keys 0
func GetRequestKey(r *http.Request, key string) string {
	return r.URL.Query().Get(key)
}